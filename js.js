/*JavaScript for image carousel.
*Buttons will display certain banners depending when clicked
*With the nav bar you can also select an option which will change the banner.*
*/


/*Banner JS variables.
*First three variables pull the banner divs to change their display.
*Sets the first banner to display block.
*Contains the first auto increment for the set interval which will act as the banner clasas number.
*Banner test gets the number of banner classes so they wont go overr the number of banners later.
*/

var banner = document.getElementsByClassName('banner')[0];
var banner1 = document.getElementsByClassName('banner 1')[0];
var banner2 = document.getElementsByClassName('banner 2')[0];
banner.style.display='block';
var auto = 1;
var bannertest = document.querySelectorAll('.banner');



//document.getElementsByClassName('test1')[0].addEventListener('click', test1);
	/*function test(){
		console.log('test2');
		if(banner.style.display='block' ){
			console.log('test3');
		    banner.style.display='none';
	        banner1.style.display='block';
	        auto = 2;
	    }else if(banner1.style.display='block') {
	    	console.log('test4');
		    banner1.style.display='none';
	        banner2.style.display='block'; 
	        auto = 3;
	    }else{
	    	console.log('test5');
		    banner2.style.display='none';
	        banner.style.display='block'; 
	        banner1.style.display='none';	
	        auto = 1;
	    }
	}*/

/*function test1(){
		//console.log('test2');
		if(auto == 1){
			console.log('test3');
		    banner.style.display='none';
	        banner1.style.display='block';
	        banner2.style.display='none';
	        auto = 2;
	    }else if(auto == 2) {
	    	console.log('test4');
	    	banner.style.display='none';
		    banner1.style.display='none';
	        banner2.style.display='block'; 
	        auto = 0;
	    }else{
	    	console.log('test5');
		    banner2.style.display='none';
	        banner.style.display='block'; 
	        banner1.style.display='none';	
	        auto = 1;
	    }

	}





document.getElementsByClassName('test2')[0].addEventListener('click', test2);
	function test2(){
		if( banner.style.display='block' ){
		    banner.style.display='none';
	        banner2.style.display='block';
	    }else if(banner1.style.display='block') {
		    banner1.style.display='none';
	        banner.style.display='block'; 
	    }else{
		    banner2.style.display='none';
	        banner1.style.display='block'; 	
	    }


	}

	*/

/*Automated swapping function
*Uses setinterval and for loop with increments.
*Sets all the banners to display none.
*Using the auto counter as the banner classes to display the class of the banner.
*Increments the banner count.
*Once the banner count gets too high, it resets back down to zero so it can never go over the amount of banners.
*This action is performed every 8 seconds, similair to other websites such as Nutrascience labs, Protien world, USN and Bulk Powders.
*Enough time for the banner show different items or adverts to users, but not enough to distract them from their goals. 
*/
setInterval(function() {    
	for(var i = 0; i < bannertest.length; i++) {
		bannertest[i].style.display = 'none';
	}  
	bannertest[auto].style.display = 'block';
	auto++;
	if (auto == bannertest.length){
		auto = 0;
	}
}, 8000);



/*
*Functions that enable the user to click on buttons to change banners.
*Two functions for each banner that then sets the banner class to the correct number.
*Banner zero functions
*/
function bannerBack() {
	banner.style.display='none';
	banner2.style.display='block';
	auto = 0;
}

function bannerForward() {
	banner.style.display='none';
	banner1.style.display='block';
	auto = 2;
}

//Banner one onclicks

function bannerOneBack() {
	banner1.style.display='none';
	banner.style.display='block';
	auto = 1;
}

function bannerOneForward() {
	banner1.style.display='none';
	banner2.style.display='block';
	auto = 0;
}

//Banner two onclicks

function bannerTwoBack() {
	banner2.style.display='none';
	banner1.style.display='block';
	auto = 2;
}

function bannerTwoForward() {
	banner2.style.display='none';
	banner.style.display='block';
	auto = 1;
}











/*var auto = [
	banner.style.display='none' + banner1.style.display='block' + banner2.style.display='none',

	banner.style.display='none'
	banner1.style.display='none'
	banner2.style.display='block',

	banner.style.display='block'
	banner1.style.display='none'
	banner2.style.display='none'


	];*/

/*for (var n = 0, rotate = auto.length; n < rotate; n+= 1){
	(function(n){
		setInterval(function(){
			auto[n] += 1;
			console.log('test');
		}, 5000)

	})(n);
}*/





/*
setTimeout(function(){ 
	if(banner1.style.display='none' && banner2.style.display='none' && banner.style.display='block' ){
		banner.style.display='none';
	    banner1.style.display='block';
	}else if(banner.style.display='none' && banner2.style.display='none' && banner1.style.display='block') {
		banner1.style.display='none';
	    banner2.style.display='block'; 
	}else{
		banner2.style.display='none';
	    banner.style.display='block'; 
	}
}, 5000);
*/
//automatically switching banners still got to do some work Perhaps a loop or while loop?
/*
if(banner1.style.display='none' && banner2.style.display='none' && banner.style.display='block' ){
	setTimeout(function(){ 
	    banner.style.display='none';
	    banner1.style.display='block'; 
    }, 5000);
}else if(banner.style.display='none' && banner2.style.display='none' && banner1.style.display='block'){
	setTimeout(function(){ 
	    banner1.style.display='none';
	    banner2.style.display='block'; 
    }, 5000);
}else{
	setTimeout(function(){ 
	    banner2.style.display='none';
	    banner.style.display='block'; 
    }, 5000);
}
*/

//attempt of loop?


/*while(banner1.style.display='none' && banner2.style.display='none' && banner.style.display='block' ){
	setTimeout(function(){ 
	    banner.style.display='none';
	    banner1.style.display='block'; 
    }, 5000);
}else if(banner.style.display='none' && banner2.style.display='none' && banner1.style.display='block'){
	setTimeout(function(){ 
	    banner1.style.display='none';
	    banner2.style.display='block'; 
    }, 5000);
}else{
	setTimeout(function(){ 
	    banner2.style.display='none';
	    banner.style.display='block'; 
    }, 5000);
}
*/



